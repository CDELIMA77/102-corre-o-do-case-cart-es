package br.com.mastertech.cartao.cartao.service;

import br.com.mastertech.cartao.cartao.exception.CartaoAlreadyExistsException;
import br.com.mastertech.cartao.cartao.exception.CartaoNotFoundException;
import br.com.mastertech.cartao.cartao.model.Cartao;
import br.com.mastertech.cartao.cartao.repository.CartaoRepository;
import br.com.mastertech.cartao.client.model.Client;
import br.com.mastertech.cartao.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClientService clientService;

    public Cartao create(Cartao cartao) {
        // Bloco de validação
        Client client = clientService.getById(cartao.getClient().getId());
        cartao.setClient(client);

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }

        // Regras de negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}
