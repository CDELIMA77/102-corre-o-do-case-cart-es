package br.com.mastertech.cartao.client.repository;

import br.com.mastertech.cartao.client.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
