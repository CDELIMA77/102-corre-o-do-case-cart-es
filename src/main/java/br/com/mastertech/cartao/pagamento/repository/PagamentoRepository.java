package br.com.mastertech.cartao.pagamento.repository;

import br.com.mastertech.cartao.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartao_id(Long cartao_id);

}
